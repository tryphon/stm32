
target remote :3333
# monitor arm semihosting enable

# After clock settup APB bus run at 27Mhz
monitor tpiu config internal /tmp/itm.fifo uart off 32000000

monitor itm port 0 on

# Better GDB defaults ----------------------------------------------------------

set history save
set verbose off
set print pretty on
set print array off
set print array-indexes on
set python print-stack full

load

