To regenerate the peripheral tree:

svd2rust -i ./STM32F7x7.svd
cat lib.rs | form -o stm32f7x7/src
cargo fmt --all