//! Rest and clock control

use stm32f7::stm32f7x7::{rcc, RCC};
use common::{Constrain, VoltageRange};
use flash::Flash;
use power::Power;
use time::{Hertz, U32Ext};

pub struct RccClock {
    rcc: RCC
}

macro_rules! rcc_peripheral_clock { ($($bus_enable_register:ident, $bus_rst_register:ident,
    [$($peripheral_clock:ident: ($enable_field:ident, $rst_field:ident)),+]),+) => {
    pub struct Rcc;

    impl Rcc {

        fn cfgr(&self) -> &rcc::CFGR {
            unsafe { &(*RCC::ptr()).cfgr }
        }

        fn ahb1en(&self) -> &rcc::AHB1ENR {
            unsafe { &(*RCC::ptr()).ahb1enr }
        }

        fn ahb1rstr(&self) -> &rcc::AHB1RSTR {
            unsafe { &(*RCC::ptr()).ahb1rstr }
        }

        fn apb1en(&self) -> &rcc::APB1ENR {
            unsafe { &(*RCC::ptr()).apb1enr }
        }

        fn apb1rstr(&self) -> &rcc::APB1RSTR {
            unsafe { &(*RCC::ptr()).apb1rstr }
        }

        pub fn is_sysclk_hsi(&self) -> bool {
            self.cfgr().read().sws().is_hsi()
        }

        pub fn is_sysclk_hse(&self) -> bool {
            self.cfgr().read().sws().is_hse()
        }

        pub fn is_sysclk_pll(&self) -> bool {
            self.cfgr().read().sws().is_pll()
        }
        $($(
            pub fn $peripheral_clock(&self) {
                if self.$bus_enable_register().read().$enable_field().is_disabled() {
                    self.$bus_enable_register().modify(|_, w| w.$enable_field().enabled());
                    self.$bus_rst_register().modify(|_, w| w.$rst_field().set_bit());
                    self.$bus_rst_register().modify(|_, w| w.$rst_field().clear_bit());
                }
            }
        )+)+
    }

}}

rcc_peripheral_clock!(ahb1en, ahb1rstr, [
    enable_gpioa: (gpioaen, gpioarst),
    enable_gpiob: (gpioben, gpiobrst),
    enable_gpioc: (gpiocen, gpiocrst),
    enable_gpiod: (gpioden, gpiodrst),
    enable_gpioe: (gpioeen, gpioerst),
    enable_gpiof: (gpiofen, gpiofrst),
    enable_gpiog: (gpiogen, gpiogrst),
    enable_gpioh: (gpiohen, gpiohrst),
    enable_gpioi: (gpioien, gpioirst),
    enable_gpioj: (gpiojen, gpiojrst),
    enable_gpiok: (gpioken, gpiokrst)
], apb1en, apb1rstr, [
    enable_usart2: (usart2en, uart2rst),
    enable_usart3: (usart3en, uart3rst),
    enable_i2c1: (i2c1en, i2c1rst),
    enable_tim6: (tim6en, tim6rst)
]);


/// Frozen clock frequencies
///
/// The existence of this value indicates that the clock configuration can no longer be changed
pub struct Clocks {
    hclk: Hertz,
    pclk: Hertz,
    sysclk: Hertz,
    frozen: RCC,
}

impl Clocks {
    /// Returns the frequency of the AHB
    pub fn hclk(&self) -> Hertz {
        self.hclk
    }

    /// Returns the frequency of the APB
    pub fn pclk(&self) -> Hertz {
        self.pclk
    }

    /// Returns the system (core) frequency
    pub fn sysclk(&self) -> Hertz {
        self.sysclk
    }

    pub fn unfreeze(self) -> RccClock {
        RccClock {
            rcc: self.frozen
        }
    }
}

impl Constrain<(Rcc, RccClock)> for RCC {
    fn constrain(self) -> (Rcc, RccClock) {
        (
            Rcc,
            RccClock {
                rcc: self
            }
        )
    }
}


impl RccClock {

    pub fn freeze(self, rss_status: &Rcc, flash: Flash, power: Power) -> Clocks {
        // Setting APBx clock setting
        self.rcc.cfgr.modify(|_, w| unsafe { 
            w.hpre().bits(0b0000) // Not divided AHB clock aka HCLK
            .ppre1().bits(0b100) // APB 1 clock / 2
            .ppre2().bits(0b100) // APB 2 clock / 2
        });

        // Activate HSE
        // Two step as hseon should be off to set the hsebyp. cf. doc.
        self.rcc.cr.modify(|_, w| w.hsebyp().bit(true));
        self.rcc.cr.modify(|_, w| w.hseon().bit(true));
        while self.rcc.cr.read().hserdy().bit_is_set() {}

        self.rcc.cr.modify(|_, w| w.pllon().bit(false));
        while self.rcc.cr.read().pllrdy().bit_is_set() {}

        flash.set_latency(32.mhz().into(), VoltageRange::Vdd27_36);

        self.rcc.pllcfgr.modify(|_, w| unsafe {
            w.pllm().bits(4) // VCO In is better at 2Mhz cf doc. (8MHz/4)
             .plln().bits(128) // VCO out 256 Mhz 
             .pllp().bits(3) // sysclk => 32 Mhz  x/8
             .pllq().bits(6) // USB OTG FS, RNG, ... clock need to be < 48Mhz 
             .pllr().bits(6) // DSI clock at 72Mhz not used.
             .pllsrc().bit(true) // Lets use HSE. 8Mhz HSI 16Mhz
        });

        self.rcc.cr.modify(|_, w| w.pllon().bit(true));

        self.rcc.apb1enr.modify(|_, w| w.pwren().bit(true));

        power.enable_over_dirve(rss_status);

        while self.rcc.cr.read().pllrdy().bit_is_clear() {}

        self.rcc.cfgr.modify(|_, w| w.sw().pll());
        while !self.rcc.cfgr.read().sws().is_pll() {}

        Clocks {
            hclk: 32.mhz().into(),
            pclk: 16.mhz().into(),
            sysclk: 32.mhz().into(),
            frozen: self.rcc
        }
    }
}


