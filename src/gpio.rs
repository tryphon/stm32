//! General Purpose Input / Output

// TODO the pins here currently correspond to the LQFP-100 package. There should be Cargo features
// that let you select different microcontroller packages

use core::marker::PhantomData;

use rcc::Rcc;

/// Extension trait to split a GPIO peripheral in independent pins and registers
pub trait GpioExt {
    /// The to split the GPIO into
    type Parts;

    /// Splits the GPIO block into independent pins and registers
    fn split(self, rcc: &mut Rcc) -> Self::Parts;
}

/// Input mode (type state)
pub struct Input<MODE> {
    _mode: PhantomData<MODE>,
}

/// Floating input (type state)
pub struct Floating;
/// Pulled down input (type state)
pub struct PullDown;
/// Pulled up input (type state)
pub struct PullUp;

/// Output mode (type state)
pub struct Output<MODE> {
    _mode: PhantomData<MODE>,
}

/// Push pull output (type state)
pub struct PushPull;
/// Open drain output (type state)
pub struct OpenDrain;

/// Alternate function 0 (type state)
pub struct AF0;

/// Alternate function 1 (type state)
pub struct AF1;

/// Alternate function 2 (type state)
pub struct AF2;

/// Alternate function 3 (type state)
pub struct AF3;

/// Alternate function 4 (type state)
pub struct AF4;

/// Alternate function 5 (type state)
pub struct AF5;

/// Alternate function 6 (type state)
pub struct AF6;

/// Alternate function 7 (type state)
pub struct AF7;

/// Alternate function 8 (type state)
pub struct AF8;

/// Alternate function 9 (type state)
pub struct AF9;

/// Alternate function 10 (type state)
pub struct AF10;

/// Alternate function 11 (type state)
pub struct AF11;

/// Alternate function 12 (type state)
pub struct AF12;

/// Alternate function 13 (type state)
pub struct AF13;

/// Alternate function 14 (type state)
pub struct AF14;

/// Alternate function 15 (type state)
pub struct AF15;

macro_rules! into_af {
    ($PXi:ident, $afr:ident, $i:expr, $into_af_func_name:ident, $AFTYPE:ty, $afvalue:expr) => {
        pub fn $into_af_func_name(
            self,
            parts: &mut CommonGpio
        ) -> $PXi<$AFTYPE> {
            let offset = 2 * $i;

            // alternate function mode
            let mode = 0b10;
            parts.moder().modify(|r, w| unsafe {
                w.bits((r.bits() & !(0b11 << offset)) | (mode << offset))
            });

            let af = $afvalue;
            let offset = 4 * ($i % 8);
            parts.$afr().modify(|r, w| unsafe {
                w.bits((r.bits() & !(0b1111 << offset)) | (af << offset))
            });

            $PXi { _mode: PhantomData }
        }

    }
}

macro_rules! gpio {
    ($GPIOX:ident, $gpiomod:ident, $gpiosvd:ident, $port_enable:ident, $PXx:ident, [
        $($PXi:ident: ($pxi:ident, $i:expr, $MODE:ty, $afr:ident, ($($func:ident -> $AF:ident -> $func_alias:ident),*)),)+
    ]) => {
        /// GPIO
        pub mod $gpiomod {
            use core::marker::PhantomData;

            use hal::digital::OutputPin;
            use stm32f7::stm32f7x7::{$gpiosvd, $GPIOX};

            use rcc::Rcc;
            use super::{
                AF1, AF2, AF3, AF4, AF5, AF6, AF7, AF8, AF9, AF10, AF11, AF12, Floating, GpioExt, Input, OpenDrain, Output,
                PullDown, PullUp, PushPull,
            };

            pub struct CommonGpio;

            /// GPIO parts
            pub struct Parts {
                pub gpio: CommonGpio,
                $(
                    /// Pin
                    pub $pxi: $PXi<$MODE>,
                )+
            }

            impl GpioExt for $GPIOX {
                type Parts = Parts;

                fn split(self, rcc: &mut Rcc) -> Parts {
                    rcc.$port_enable();

                    Parts {
                        gpio: CommonGpio,
                        $(
                            $pxi: $PXi { _mode: PhantomData },
                        )+
                    }
                }
            }

            impl CommonGpio {
                fn moder(&mut self) -> &$gpiosvd::MODER {
                    unsafe { &(*$GPIOX::ptr()).moder }
                }

                fn pupdr(&mut self) -> &$gpiosvd::PUPDR {
                    unsafe { &(*$GPIOX::ptr()).pupdr }
                }

                fn otyper(&mut self) -> &$gpiosvd::OTYPER {
                    unsafe { &(*$GPIOX::ptr()).otyper }
                }

                fn afrl(&mut self) -> &$gpiosvd::AFRL {
                    unsafe { &(*$GPIOX::ptr()).afrl }
                }

                fn afrh(&mut self) -> &$gpiosvd::AFRH {
                    unsafe { &(*$GPIOX::ptr()).afrh }
                }
            }

            /// Partially erased pin
            pub struct $PXx<MODE> {
                i: u8,
                _mode: PhantomData<MODE>,
            }

            impl<MODE> OutputPin for $PXx<Output<MODE>> {
                fn set_high(&mut self) {
                    // NOTE(unsafe) atomic write to a stateless register
                    unsafe { (*$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << self.i)) }
                }

                fn set_low(&mut self) {
                    // NOTE(unsafe) atomic write to a stateless register
                    unsafe { (*$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << (16 + self.i))) }
                }
            }



            $(
                /// Pin
                pub struct $PXi<MODE> {
                    _mode: PhantomData<MODE>,
                }

                impl<MODE> $PXi<MODE> {

                    into_af!($PXi, $afr, $i, into_af1, AF1, 1);
                    into_af!($PXi, $afr, $i, into_af2, AF2, 2);
                    into_af!($PXi, $afr, $i, into_af3, AF3, 3);
                    into_af!($PXi, $afr, $i, into_af4, AF4, 4);
                    into_af!($PXi, $afr, $i, into_af5, AF5, 5);
                    into_af!($PXi, $afr, $i, into_af6, AF6, 6);
                    into_af!($PXi, $afr, $i, into_af7, AF7, 7);
                    into_af!($PXi, $afr, $i, into_af8, AF8, 8);
                    into_af!($PXi, $afr, $i, into_af9, AF9, 9);
                    into_af!($PXi, $afr, $i, into_af10, AF10, 10);
                    into_af!($PXi, $afr, $i, into_af11, AF11, 11);
                    into_af!($PXi, $afr, $i, into_af12, AF12, 12);

                    $(
                        pub fn $func_alias(
                            self,
                            parts: &mut CommonGpio
                        ) -> $PXi<$AF> {
                            self.$func(parts)
                        }
                    )*

                    /// Configures the pin to operate as a floating input pin
                    pub fn into_floating_input(
                        self,
                        parts: &mut CommonGpio,
                    ) -> $PXi<Input<Floating>> {
                        let offset = 2 * $i;

                        // input mode
                        parts
                            .moder()
                            .modify(|r, w| unsafe { w.bits(r.bits() & !(0b11 << offset)) });

                        // no pull-up or pull-down
                        parts
                            .pupdr()
                            .modify(|r, w| unsafe { w.bits(r.bits() & !(0b11 << offset)) });

                        $PXi { _mode: PhantomData }
                    }

                    /// Configures the pin to operate as a pulled down input pin
                    pub fn into_pull_down_input(
                        self,
                        parts: &mut CommonGpio,
                    ) -> $PXi<Input<PullDown>> {
                        let offset = 2 * $i;

                        // input mode
                        parts
                            .moder()
                            .modify(|r, w| unsafe { w.bits(r.bits() & !(0b11 << offset)) });

                        // pull-down
                        parts.pupdr().modify(|r, w| unsafe {
                            w.bits((r.bits() & !(0b11 << offset)) | (0b10 << offset))
                        });

                        $PXi { _mode: PhantomData }
                    }

                    /// Configures the pin to operate as a pulled up input pin
                    pub fn into_pull_up_input(
                        self,
                        parts: &mut CommonGpio,
                    ) -> $PXi<Input<PullUp>> {
                        let offset = 2 * $i;

                        // input mode
                        parts.moder().modify(|r, w| unsafe { 
                            w.bits(r.bits() & !(0b11 << offset)) 
                        });

                        // pull-up
                        parts.pupdr().modify(|r, w| unsafe {
                            w.bits((r.bits() & !(0b11 << offset)) | (0b01 << offset))
                        });

                        $PXi { _mode: PhantomData }
                    }

                    /// Configures the pin to operate as an open drain output pin
                    pub fn into_open_drain_output(
                        self,
                        parts: &mut CommonGpio,
                    ) -> $PXi<Output<OpenDrain>> {
                        let offset = 2 * $i;

                        // general purpose output mode
                        let mode = 0b01;
                        parts.moder().modify(|r, w| unsafe {
                            w.bits((r.bits() & !(0b11 << offset)) | (mode << offset))
                        });

                        // open drain output
                        parts.otyper().modify(|r, w| unsafe {
                            w.bits(r.bits() | (0b1 << $i))
                        });

                        $PXi { _mode: PhantomData }
                    }

                    /// Configures the pin to operate as an push pull output pin
                    pub fn into_push_pull_output(
                        self,
                        parts: &mut CommonGpio,
                    ) -> $PXi<Output<PushPull>> {
                        let offset = 2 * $i;

                        // general purpose output mode
                        let mode = 0b01;
                        parts.moder().modify(|r, w| unsafe {
                            w.bits((r.bits() & !(0b11 << offset)) | (mode << offset))
                        });

                        // push pull output
                        parts.otyper().modify(|r, w| unsafe {
                            w.bits(r.bits() & !(0b1 << $i))
                        });

                        $PXi { _mode: PhantomData }
                    }
                }

                impl $PXi<Output<OpenDrain>> {
                    /// Enables / disables the internal pull up
                    pub fn internal_pull_up(&mut self, parts: &mut CommonGpio, on: bool) {
                        let offset = 2 * $i;

                        parts.pupdr().modify(|r, w| unsafe {
                            w.bits(
                                (r.bits() & !(0b11 << offset)) | if on {
                                    0b01 << offset
                                } else {
                                    0
                                },
                            )
                        });
                    }
                }

                impl<MODE> $PXi<Output<MODE>> {
                    /// Erases the pin number from the type
                    ///
                    /// This is useful when you want to collect the pins into an array where you
                    /// need all the elements to have the same type
                    pub fn downgrade(self) -> $PXx<Output<MODE>> {
                        $PXx {
                            i: $i,
                            _mode: self._mode,
                        }
                    }
                }

                impl<MODE> OutputPin for $PXi<Output<MODE>> {
                    fn set_high(&mut self) {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe { (*$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << $i)) }
                    }

                    fn set_low(&mut self) {
                        // NOTE(unsafe) atomic write to a stateless register
                        unsafe { (*$GPIOX::ptr()).bsrr.write(|w| w.bits(1 << (16 + $i))) }
                    }
                }
            )+
        }
    }
}

gpio!(GPIOA, gpioa, gpioa, enable_gpioa, PAx, [
    PA0: (pa0, 0, Input<Floating>, afrl, (into_af8 -> AF8 -> into_usart4_tx)),
    PA1: (pa1, 1, Input<Floating>, afrl, (into_af8 -> AF8 -> into_usart4_rx)),
    PA2: (pa2, 2, Input<Floating>, afrl, ()),
    PA3: (pa3, 3, Input<Floating>, afrl, ()),
    PA4: (pa4, 4, Input<Floating>, afrl, ()),
    PA5: (pa5, 5, Input<Floating>, afrl, ()),
    PA6: (pa6, 6, Input<Floating>, afrl, ()),
    PA7: (pa7, 7, Input<Floating>, afrl, ()),
    PA8: (pa8, 8, Input<Floating>, afrh, (into_af12 -> AF12 -> into_usart7_rx)),
    PA9: (pa9, 9, Input<Floating>, afrh, ()),
    PA10: (pa10, 10, Input<Floating>, afrh, ()),
    PA11: (pa11, 11, Input<Floating>, afrh, (into_af6 -> AF6 -> into_usart4_rx)),
    PA12: (pa12, 12, Input<Floating>, afrh, (into_af6 -> AF6 -> into_usart4_tx)),
    // TODO these are configured as JTAG pins
    // PA13: (13, Input<Floating>),
    // PA14: (14, Input<Floating>),
    // PA15: (15, Input<Floating>, (into_af12 -> AF12 -> into_usart7_tx)),
]);

gpio!(GPIOB, gpiob, gpiob, enable_gpiob, PBx, [
    PB0: (pb0, 0, Input<Floating>, afrl, ()),
    PB1: (pb1, 1, Input<Floating>, afrl, ()),
    PB2: (pb2, 2, Input<Floating>, afrl, ()),
    // TODO these are configured as JTAG pins
    // PB3: (3, Input<Floating>),
    // PB4: (4, Input<Floating>),
    PB5: (pb5, 5, Input<Floating>, afrl, (into_af1 -> AF1 -> into_uart5_rx)),
    PB6: (pb6, 6, Input<Floating>, afrl, (into_af1 -> AF1 -> into_uart5_tx)),
    PB7: (pb7, 7, Input<Floating>, afrl, ()),
    PB8: (pb8, 8, Input<Floating>, afrh, (into_af7 -> AF7 -> into_uart5_rx, into_af4 -> AF4 -> into_i2c1_scl)),
    PB9: (pb9, 9, Input<Floating>, afrh, (into_af7 -> AF7 -> into_uart5_tx, into_af4 -> AF4 -> into_i2c1_sda)),
    PB10: (pb10, 10, Input<Floating>, afrh, ()),
    PB11: (pb11, 11, Input<Floating>, afrh, ()),
    PB12: (pb12, 12, Input<Floating>, afrh, ()),
    PB13: (pb13, 13, Input<Floating>, afrh, ()),
    PB14: (pb14, 14, Input<Floating>, afrh, ()),
    PB15: (pb15, 15, Input<Floating>, afrh, ()),
]);

gpio!(GPIOC, gpioc, gpiod, enable_gpioc, PCx, [
    PC0: (pc0, 0, Input<Floating>, afrl, ()),
    PC1: (pc1, 1, Input<Floating>, afrl, ()),
    PC2: (pc2, 2, Input<Floating>, afrl, ()),
    PC3: (pc3, 3, Input<Floating>, afrl, ()),
    PC4: (pc4, 4, Input<Floating>, afrl, ()),
    PC5: (pc5, 5, Input<Floating>, afrl, ()),
    PC6: (pc6, 6, Input<Floating>, afrl, ()),
    PC7: (pc7, 7, Input<Floating>, afrl, ()),
    PC8: (pc8, 8, Input<Floating>, afrh, ()),
    PC9: (pc9, 9, Input<Floating>, afrh, ()),
    PC10: (pc10, 10, Input<Floating>, afrh, ()),
    PC11: (pc11, 11, Input<Floating>, afrh, (into_af8 -> AF8 -> into_uart5_rx)),
    PC12: (pc12, 12, Input<Floating>, afrh, (into_af8 -> AF8 -> into_uart5_tx)),
    PC13: (pc13, 13, Input<Floating>, afrh, ()),
    PC14: (pc14, 14, Input<Floating>, afrh, ()),
    PC15: (pc15, 15, Input<Floating>, afrh, ()),
]);

gpio!(GPIOD, gpiod, gpiod, enable_gpiod, PDx, [
    PD0: (pd0, 0, Input<Floating>, afrl, (into_af8 -> AF8 -> into_uart4_rx)),
    PD1: (pd1, 1, Input<Floating>, afrl, (into_af8 -> AF8 -> into_uart4_tx)),
    PD2: (pd2, 2, Input<Floating>, afrl, ()),
    PD3: (pd3, 3, Input<Floating>, afrl, ()),
    PD4: (pd4, 4, Input<Floating>, afrl, ()),
    PD5: (pd5, 5, Input<Floating>, afrl, (into_af7 -> AF7 -> into_usart2_tx)),
    PD6: (pd6, 6, Input<Floating>, afrl, (into_af7 -> AF7 -> into_usart2_rx)),
    PD7: (pd7, 7, Input<Floating>, afrl, ()),
    PD8: (pd8, 8, Input<Floating>, afrh, (into_af7 -> AF7 -> into_usart3_tx)),
    PD9: (pd9, 9, Input<Floating>, afrh, (into_af7 -> AF7 -> into_usart3_rx)),
    PD10: (pd10, 10, Input<Floating>, afrh, ()),
    PD11: (pd11, 11, Input<Floating>, afrh, ()),
    PD12: (pd12, 12, Input<Floating>, afrh, ()),
    PD13: (pd13, 13, Input<Floating>, afrh, ()),
    PD14: (pd14, 14, Input<Floating>, afrh, ()),
    PD15: (pd15, 15, Input<Floating>, afrh, ()),
]);

gpio!(GPIOE, gpioe, gpiod, enable_gpioe, PEx, [
    PE0: (pe0, 0, Input<Floating>, afrl, (into_af8 -> AF8 -> into_uart8_rx)),
    PE1: (pe1, 1, Input<Floating>, afrl, (into_af8 -> AF8 -> into_uart8_tx)),
    PE2: (pe2, 2, Input<Floating>, afrl, ()),
    PE3: (pe3, 3, Input<Floating>, afrl, ()),
    PE4: (pe4, 4, Input<Floating>, afrl, ()),
    PE5: (pe5, 5, Input<Floating>, afrl, ()),
    PE6: (pe6, 6, Input<Floating>, afrl, ()),
    PE7: (pe7, 7, Input<Floating>, afrl, (into_af8 -> AF8 -> into_uart7_rx)),
    PE8: (pe8, 8, Input<Floating>, afrh, (into_af8 -> AF8 -> into_uart7_tx)),
    PE9: (pe9, 9, Input<Floating>, afrh, ()),
    PE10: (pe10, 10, Input<Floating>, afrh, ()),
    PE11: (pe11, 11, Input<Floating>, afrh, ()),
    PE12: (pe12, 12, Input<Floating>, afrh, ()),
    PE13: (pe13, 13, Input<Floating>, afrh, ()),
    PE14: (pe14, 14, Input<Floating>, afrh, ()),
    PE15: (pe15, 15, Input<Floating>, afrh, ()),
]);

gpio!(GPIOF, gpiof, gpiod, enable_gpiof, PFx, [
    PF0: (pf0, 0, Input<Floating>, afrl, ()),
    PF1: (pf1, 1, Input<Floating>, afrl, ()),
    PF2: (pf2, 2, Input<Floating>, afrl, ()),
    PF4: (pf3, 4, Input<Floating>, afrl, ()),
    PF6: (pf6, 6, Input<Floating>, afrl, ()),
    PF9: (pf9, 9, Input<Floating>, afrh, ()),
    PF10: (pf10, 10, Input<Floating>, afrh, ()),
]);