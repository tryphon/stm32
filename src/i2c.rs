use hal::blocking::i2c::{Write, WriteRead};
use stm32f7::stm32f7x7::{I2C1};
use rcc::{Rcc, Clocks};
use gpio::{AF4};
use gpio::gpiob::{PB8, PB9};
use time::Hertz;

macro_rules! busy_wait {
    ($i2c:expr, $flag:ident) => {
        loop {
            let isr = $i2c.isr.read();

            if isr.berr().bit_is_set() {
                return Err(Error::Bus);
            } else if isr.arlo().bit_is_set() {
                return Err(Error::Arbitration);
            } else if isr.$flag().bit_is_set() {
                break;
            } else {
                // try again
            }
        }
    };
}


/// I2C error
#[derive(Debug)]
pub enum Error {
    /// Bus error
    Bus,
    /// Arbitration loss
    Arbitration,
    // Overrun, // slave mode only
    // Pec, // SMBUS mode only
    // Timeout, // SMBUS mode only
    // Alert, // SMBUS mode only
    #[doc(hidden)]
    _Extensible,
}


pub struct I2c {
    i2c: I2C1,
    pins: (PB8<AF4>, PB9<AF4>),
}


impl I2c {

    pub fn i2c1(i2c: I2C1, scl: PB8<AF4>, sda: PB9<AF4>, rcc: &mut Rcc) -> I2c {
        // At some point we would need rcc.
        // Let assum that we are running i2c on APB1 at 16Mhz.
        rcc.enable_i2c1();
        i2c.timingr.write(|w| unsafe {
            w.presc()
                .bits(3)
                .scll()
                .bits(0x13)
                .sclh()
                .bits(0xF)
                .sdadel()
                .bits(2)
                .scldel()
                .bits(4)
        });

        i2c.cr1.write(|w| w.pe().set_bit());

        I2c {i2c, pins: (scl, sda)}
    }

}

impl Write for I2c {

    type Error = Error;

    fn write(&mut self, addr: u8, bytes: &[u8]) -> Result<(), Error> {
        // TODO support transfers of more than 255 bytes
        assert!(bytes.len() < 256 && bytes.len() > 0);

        // START and prepare to send `bytes`
        self.i2c.cr2.write(|w| unsafe {
            w.sadd()
                .bits(addr.into())
                .rd_wrn()
                .clear_bit()
                .nbytes()
                .bits(bytes.len() as u8)
                .start()
                .set_bit()
                .autoend()
                .set_bit()
        });

        for byte in bytes {
            // Wait until we are allowed to send data (START has been ACKed or last byte
            // when through)
            busy_wait!(self.i2c, txis);

            // put byte on the wire
            self.i2c.txdr.write(|w| unsafe { w.txdata().bits(*byte)} );
        }

        // Wait until the last transmission is finished ???
        // busy_wait!(self.i2c, busy);

        // automatic STOP

        Ok(())
    }

}

impl WriteRead for I2c {
    type Error = Error;

    fn write_read(
        &mut self,
        addr: u8,
        bytes: &[u8],
        buffer: &mut [u8],
    ) -> Result<(), Error> {
        // TODO support transfers of more than 255 bytes
        assert!(bytes.len() < 256 && bytes.len() > 0);
        assert!(buffer.len() < 256 && buffer.len() > 0);

        // TODO do we have to explicitly wait here if the bus is busy (e.g. another
        // master is communicating)?

        // START and prepare to send `bytes`
        self.i2c.cr2.write(|w| unsafe {
            w.sadd()
                .bits(addr.into())
                .rd_wrn()
                .clear_bit()
                .nbytes()
                .bits(bytes.len() as u8)
                .start()
                .set_bit()
                .autoend()
                .clear_bit()
        });

        for byte in bytes {
            // Wait until we are allowed to send data (START has been ACKed or last byte
            // when through)
            busy_wait!(self.i2c, txis);

            // put byte on the wire
            self.i2c.txdr.write(|w| unsafe { w.txdata().bits(*byte) });
        }

        // Wait until the last transmission is finished
        busy_wait!(self.i2c, tc);

        // reSTART and prepare to receive bytes into `buffer`
        self.i2c.cr2.write(|w| unsafe {
            w.sadd()
                .bits(addr.into())
                .rd_wrn()
                .set_bit()
                .nbytes()
                .bits(buffer.len() as u8)
                .start()
                .set_bit()
                .autoend()
                .set_bit()
        });

        for byte in buffer {
            // Wait until we have received something
            busy_wait!(self.i2c, rxne);

            *byte = self.i2c.rxdr.read().rxdata().bits();
        }

        // automatic STOP

        Ok(())
    }
}
