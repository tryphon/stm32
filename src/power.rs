//! Power control

use stm32f7::stm32f7x7::{pwr, PWR};
use rcc::Rcc;
use common::Constrain;

/// Constrained Power control module
pub struct Power {
    /// Power register
    pwr: PWR,
}

pub struct OverDrivePower {
    pwr: PWR,
    previous_vos: pwr::cr1::VOSR,
}

impl Constrain<Power> for PWR {
    fn constrain(self) -> Power {
        Power {
            pwr: self,
        }
    }
}

impl OverDrivePower {
    fn from(pwr: Power, rcc_status: &Rcc) -> OverDrivePower {

        assert_eq!(rcc_status.is_sysclk_pll(), false, "Only possible if HSE or HSE is select as SYSCLK.");
        assert_eq!(pwr.pwr.cr1.read().vos().is_scale3(), false, "Over-drive only possible when VOS scale < 3");

        let previous_vos = pwr.vos();
        pwr.pwr.cr1.modify(|_, w| w.vos().scale1());
        while pwr.pwr.csr1.read().vosrdy().bit_is_clear() {}

        pwr.pwr.cr1.modify(|_, w| w.oden().bit(true));
        while pwr.pwr.csr1.read().odrdy().bit_is_clear() {}

        pwr.pwr.cr1.modify(|_, w| w.odswen().bit(true));
        while pwr.pwr.csr1.read().odswrdy().bit_is_clear() {}

        OverDrivePower {
            pwr: pwr.pwr,
            previous_vos
        }
    }

    pub fn enable_normal_mode(self, rcc_status: &Rcc) -> Power {      

        Power::from(self, rcc_status)
    }
}

impl Power {

    fn from(pwr: OverDrivePower, rcc_status: &Rcc) -> Power {

        assert_eq!(rcc_status.is_sysclk_pll(), false, "Only possible if HSE or HSE is select as SYSCLK.");

        pwr.pwr.cr1.modify(|_, w| 
            w.odswen().bit(true)
             .oden().bit(true)
        );
        while pwr.pwr.csr1.read().odswrdy().bit_is_clear() {}

        pwr.pwr.cr1.modify(|_, w| unsafe { w.vos().bits(pwr.previous_vos.bits()) });
        while pwr.pwr.csr1.read().vosrdy().bit_is_set() {}
        Power {
            pwr: pwr.pwr
        }
    }

    pub fn enable_over_dirve(self, rcc_status: &Rcc) -> OverDrivePower {
        OverDrivePower::from(self, rcc_status)       
    }

    pub fn vos(&self) -> pwr::cr1::VOSR {
        self.pwr.cr1.read().vos()
    }

    pub fn scale1(&self) {
        self.pwr.cr1.modify(|_, w| w.vos().scale1());
        while self.pwr.csr1.read().vosrdy().bit_is_set() {}
    }
}