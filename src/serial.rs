use core::ptr;
use stm32f7::stm32f7x7::{USART3};
use gpio::{AF7};
use gpio::gpiod::{PD8, PD9};
use rcc::{Clocks, Rcc};
use time::Bps;
use nb;
use void::Void;
use hal::serial;

/// Serial error
#[derive(Debug)]
pub enum Error {
    /// Framing error
    Framing,
    /// Noise error
    Noise,
    /// RX buffer overrun
    Overrun,
    /// Parity check error
    Parity,
    #[doc(hidden)]
    _Extensible,
}

pub struct Serial {
    usart: USART3,
    pins: (PD8<AF7>, PD9<AF7>),
}

/// Serial receiver
pub struct Rx;

/// Serial transmitter
pub struct Tx;

impl Serial {
    /// Configures a USART peripheral to provide serial communication
    pub fn usart3(
        usart: USART3,
        tx: PD8<AF7>,
        rx: PD9<AF7>,
        baud_rate: Bps,
        clocks: Clocks,
        rcc: &mut Rcc
    ) -> Self {
        rcc.enable_usart3();

        // RCC_DCKCFGR2.USART2SEL is APB1 APB2 by default
        let brr = clocks.pclk().0 / baud_rate.0;
        
        usart.brr.modify(|_, w| unsafe { w.bits(brr) });
        usart.cr1.modify(|_, w| 
            w.ue().set_bit().re().set_bit().te().set_bit()
        );
        Serial { usart, pins: (tx,rx) }
    }

    /// Splits the `Serial` abstraction into a transmitter and a receiver half
    pub fn split(self) -> (Tx, Rx) {
        (
            Tx,
            Rx,
        )
    }
}

impl serial::Read<u8> for Rx {
    type Error = Error;

    fn read(&mut self) -> nb::Result<u8, Error> {
        // NOTE(unsafe) atomic read with no side effects
        let isr = unsafe { (*USART3::ptr()).isr.read() };

        Err(if isr.pe().bit_is_set() {
            nb::Error::Other(Error::Parity)
        } else if isr.fe().bit_is_set() {
            nb::Error::Other(Error::Framing)
        } else if isr.nf().bit_is_set() {
            nb::Error::Other(Error::Noise)
        } else if isr.ore().bit_is_set() {
            nb::Error::Other(Error::Overrun)
        } else if isr.rxne().bit_is_set() {
            // NOTE(read_volatile) see `write_volatile` below
            return Ok(unsafe {
                ptr::read_volatile(&(*USART3::ptr()).rdr as *const _ as *const _)
            });
        } else {
            nb::Error::WouldBlock
        })
    }
}

impl serial::Write<u8> for Tx {
    // NOTE(Void) See section "29.7 USART interrupts"; the only possible errors during
    // transmission are: clear to send (which is disabled in this case) errors and
    // framing errors (which only occur in SmartCard mode); neither of these apply to
    // our hardware configuration
    type Error = Void;

    fn flush(&mut self) -> nb::Result<(), Void> {
        // NOTE(unsafe) atomic read with no side effects
        let isr = unsafe { (*USART3::ptr()).isr.read() };

        if isr.tc().bit_is_set() {
            Ok(())
        } else {
            Err(nb::Error::WouldBlock)
        }
    }

    fn write(&mut self, byte: u8) -> nb::Result<(), Void> {
        // NOTE(unsafe) atomic read with no side effects
        let isr = unsafe { (*USART3::ptr()).isr.read() };

        if isr.txe().bit_is_set() {
            // NOTE(unsafe) atomic write to stateless register
            // NOTE(write_volatile) 8-bit write that's not possible through the svd2rust API
            unsafe {
                ptr::write_volatile(&(*USART3::ptr()).tdr as *const _ as *mut _, byte)
            }
            Ok(())
        } else {
            Err(nb::Error::WouldBlock)
        }
    }
}