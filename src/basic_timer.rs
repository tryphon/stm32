use stm32f7::stm32f7x7::{tim6, TIM6};
use common::Constrain;



impl Constrain<Timer> for TIM6 {
    fn constrain(self) -> Timer {
        Timer {}
    }
}

pub struct Timer;

impl Timer {

    fn arr(&self) -> &tim6::ARR {
        unsafe { &(*TIM6::ptr()).arr }
    }

    fn psc(&self) -> &tim6::PSC {
        unsafe { &(*TIM6::ptr()).psc }
    }

    fn cr1(&self) -> &tim6::CR1 {
        unsafe { &(*TIM6::ptr()).cr1 }
    }

    fn dier(&self) -> &tim6::DIER {
        unsafe { &(*TIM6::ptr()).dier }
    }

    fn sr(&self) -> &tim6::SR {
        unsafe { &(*TIM6::ptr()).sr }
    }
    
    pub fn auto_reload_counter(&mut self, value: u16) {
        self.arr().write(|w| unsafe { w.arr().bits(value) })
    }

    pub fn prescaler(&mut self, value: u16) {
        self.psc().write(|w| unsafe { w.psc().bits(value) })
    }

    pub fn enable_timer(&mut self) {
        self.cr1().modify(|_, w| w.cen().set_bit())
    }

    pub fn is_counter_reset(&mut self) -> bool {
        self.sr().read().uif().bit_is_set()
    }

    pub fn clear_interupt_flag(&mut self) {
        self.sr().modify(|_, w| w.uif().clear_bit())
    }


    pub fn enable_update_interupt(&mut self) {
        self.cr1().modify(|_,w| w.urs().set_bit().arpe().set_bit());
        self.dier().modify(|_, w| w.uie().set_bit())
    }
}