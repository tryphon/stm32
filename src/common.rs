//! Common primitives for this HAL
use core::fmt::Write as W;
use core::fmt;

/// Extension trait to constrain the peripheral.
pub trait Constrain<T> {
    /// Constrains the peripheral to play nicely with the other abstractions
    fn constrain(self) -> T;
}

// Indicate the Vdd range.
pub enum VoltageRange {
    // Range from 2.7 to 3.6
    Vdd27_36,
    // Range from 2.4 to 2.7
    Vdd24_27,
    // Range from 2.1 to 2.4
    Vdd21_24,
    // Range from 1.8 to 2.1
    Vdd18_21,
}


pub struct Wrapper<'a> {
    buf: &'a mut [u8],
    offset: usize,
}

impl<'a> Wrapper<'a> {
    pub fn new(buf: &'a mut [u8]) -> Self {
        Wrapper {
            buf: buf,
            offset: 0,
        }
    }
}

impl<'a> W for Wrapper<'a> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let bytes = s.as_bytes();

        if (bytes.len() + self.offset) > self.buf.len() {
            Err(fmt::Error)
        } else {

            // Skip over already-copied data
            let remainder = &mut self.buf[self.offset..];
            // Make the two slices the same length
            let remainder = &mut remainder[..bytes.len()];
            // Copy
            remainder.copy_from_slice(bytes);

            self.offset += bytes.len();

            Ok(())
        }
    }
}