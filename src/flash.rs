//! Flash memory

use stm32f7::stm32f7x7::{flash, FLASH};
use stm32f7::stm32f7x7::flash::acr::{LATENCYW, LATENCYR};
use common::{Constrain, VoltageRange};
use time::{Hertz, MegaHertz};

/// Constrained FLASH peripheral
pub struct Flash;

impl Constrain<Flash> for FLASH {
    fn constrain(self) -> Flash {
        Flash {
        }
    }
}

impl Flash {

    fn acr(&self) -> &flash::ACR {
        unsafe { &(*FLASH::ptr()).acr }
    }

    pub fn set_latency(&self, hclk: Hertz, vdd: VoltageRange) {
        let (latencyw, latencyr) = match vdd {
            VoltageRange::Vdd18_21 => self.lowest_voltage_latency(hclk.into()),
            VoltageRange::Vdd21_24 => self.low_voltage_latency(hclk.into()),
            VoltageRange::Vdd24_27 => self.high_voltage_latency(hclk.into()),
            VoltageRange::Vdd27_36 => self.highest_voltage_latency(hclk.into()),
        };
        self.acr().modify(|_, w| w.latency().variant(latencyw) );
        while self.acr().read().latency() != latencyr {}
    }

    fn lowest_voltage_latency(&self, hclk: MegaHertz) -> (LATENCYW, LATENCYR) {
        assert!(hclk.0 < 180, "Frequency in this Vdd range can not exceed 180Mhz.");
        if hclk.0 < 20 {
            (LATENCYW::WS0, LATENCYR::WS0)
        } else if hclk.0 <= 40 {
            (LATENCYW::WS1, LATENCYR::WS1)
        } else if hclk.0 <= 60 {
            (LATENCYW::WS2, LATENCYR::WS2)
        } else if hclk.0 <= 80 {
            (LATENCYW::WS3, LATENCYR::WS3)
        } else if hclk.0 <= 100 {
            (LATENCYW::WS4, LATENCYR::WS4)
        } else if hclk.0 <= 120 {
            (LATENCYW::WS5, LATENCYR::WS5)
        } else if hclk.0 <= 140 {
            (LATENCYW::WS6, LATENCYR::WS6)
        } else if hclk.0 <= 160 {
            (LATENCYW::WS7, LATENCYR::WS7)
        } else {
            (LATENCYW::WS8, LATENCYR::WS8)
        }
    }

    fn low_voltage_latency(&self, hclk: MegaHertz) -> (LATENCYW, LATENCYR) {
        if hclk.0 < 22 {
            (LATENCYW::WS0, LATENCYR::WS0)
        } else if hclk.0 <= 44 {
            (LATENCYW::WS1, LATENCYR::WS1)
        } else if hclk.0 <= 66 {
            (LATENCYW::WS2, LATENCYR::WS2)
        } else if hclk.0 <= 88 {
            (LATENCYW::WS3, LATENCYR::WS3)
        } else if hclk.0 <= 110 {
            (LATENCYW::WS4, LATENCYR::WS4)
        } else if hclk.0 <= 132 {
            (LATENCYW::WS5, LATENCYR::WS5)
        } else if hclk.0 <= 154 {
            (LATENCYW::WS6, LATENCYR::WS6)
        } else if hclk.0 <= 176 {
            (LATENCYW::WS7, LATENCYR::WS7)
        } else if hclk.0 <= 198 {
            (LATENCYW::WS8, LATENCYR::WS8)
        } else {
            (LATENCYW::WS9, LATENCYR::WS9)
        }
    }

    fn high_voltage_latency(&self, hclk: MegaHertz) -> (LATENCYW, LATENCYR) {
        if hclk.0 < 24 {
            (LATENCYW::WS0, LATENCYR::WS0)
        } else if hclk.0 <= 48 {
            (LATENCYW::WS1, LATENCYR::WS1)
        } else if hclk.0 <= 72 {
            (LATENCYW::WS2, LATENCYR::WS2)
        } else if hclk.0 <= 96 {
            (LATENCYW::WS3, LATENCYR::WS3)
        } else if hclk.0 <= 120 {
            (LATENCYW::WS4, LATENCYR::WS4)
        } else if hclk.0 <= 144 {
            (LATENCYW::WS5, LATENCYR::WS5)
        } else if hclk.0 <= 168 {
            (LATENCYW::WS6, LATENCYR::WS6)
        } else if hclk.0 <= 192 {
            (LATENCYW::WS7, LATENCYR::WS7)
        } else {
            (LATENCYW::WS8, LATENCYR::WS8)
        }
    }

    fn highest_voltage_latency(&self, hclk: MegaHertz) -> (LATENCYW, LATENCYR) {
        if hclk.0 < 30 {
            (LATENCYW::WS0, LATENCYR::WS0)
        } else if hclk.0 <= 60 {
            (LATENCYW::WS1, LATENCYR::WS1)
        } else if hclk.0 <= 90 {
            (LATENCYW::WS2, LATENCYR::WS2)
        } else if hclk.0 <= 120 {
            (LATENCYW::WS3, LATENCYR::WS3)
        } else if hclk.0 <= 150 {
            (LATENCYW::WS4, LATENCYR::WS4)
        } else if hclk.0 <= 180 {
            (LATENCYW::WS5, LATENCYR::WS5)
        } else if hclk.0 <= 210 {
            (LATENCYW::WS6, LATENCYR::WS6)
        } else {
            (LATENCYW::WS7, LATENCYR::WS7)
        }
    }
}