#![no_main]
#![no_std]
#![feature(core_intrinsics, asm)]

#[macro_use(entry, exception)]
extern crate cortex_m_rt as rt;
#[macro_use]
extern crate cortex_m;
extern crate cortex_m_semihosting;
extern crate panic_semihosting;

#[macro_use(block)]
extern crate nb;

#[macro_use(interrupt)]
extern crate stm32f7;
extern crate stm32f7_hal;
extern crate embedded_hal;
extern crate hts221;

use stm32f7_hal::common::{Constrain, Wrapper};
use stm32f7_hal::gpio::{GpioExt, Output, PushPull};
use stm32f7_hal::gpio::gpiob::PB14;
use stm32f7_hal::serial::{Serial, Tx};
use stm32f7_hal::i2c::{I2c};
use stm32f7_hal::basic_timer::Timer;
use stm32f7_hal::time::{Bps, U32Ext};
use embedded_hal::digital::OutputPin;
use embedded_hal::serial::Write;
use stm32f7::stm32f7x7;
use rt::ExceptionFrame;
use core::fmt::Write as W;
use hts221::Hts221;
use core::ptr;


// define the hard fault handler
//exception!(SysTick, periodic);
exception!(HardFault, hard_fault);
fn hard_fault(ef: &ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}

// define the default exception handler
exception!(*, default_handler);
fn default_handler(irqn: i16) {
    panic!("unhandled exception (IRQn={})", irqn);
}

//interrupt!(TIM2, periodic);

static mut LED_RED : Option<PB14<Output<PushPull>>> = None;
static mut LED_IS_HIGH : bool = true;
static mut STIM : Option<cortex_m::peripheral::ITM> = None;
static mut TIMER : Option<Timer> = None;

interrupt!(TIM6_DAC, periodic);


fn periodic() {
    unsafe {
        if let Some(mut tim6) = ptr::read(&TIMER) {
            if let Some(mut ledRed) = ptr::read(&LED_RED) {
                if LED_IS_HIGH {
                    ledRed.set_low();
                } else {
                    ledRed.set_high();
                }
            }
            LED_IS_HIGH = !LED_IS_HIGH;
            tim6.clear_interupt_flag();
        }
    }
}

fn print_rcc_block(stim: &mut cortex_m::peripheral::itm::Stim, rcc: &stm32f7x7::RCC) {
    iprintln!(stim, "RCC_CR:0x{:08X}", rcc.cr.read().bits());
    iprintln!(stim, "RCC_PLLCFG:0x{:08X}", rcc.pllcfgr.read().bits());
    iprintln!(stim, "RCC_CFG:0x{:08X}", rcc.cfgr.read().bits());
}

fn delay(count: u32) {
    let mut c: u32 = 0;
    while c < count {
        c = c + 1;
    }
}

fn write_str(out:&mut Tx, buff: &str) {
    for c in buff.as_bytes().iter() {
        if *c == 0 { break;}
        block!(out.write(*c));
    }
}

fn write_buff(out:&mut Tx, buff: &[u8]) {
    for c in buff.iter() {
        if *c == 0 { break;}
        block!(out.write(*c));
    }
}


entry!(main);
fn main() -> ! {
    // initialization
    let p = stm32f7x7::Peripherals::take().unwrap();
    let mut pc = cortex_m::Peripherals::take().unwrap();
    let rcc = p.RCC;
    let flash = p.FLASH.constrain();
    let power = p.PWR.constrain();

    unsafe { STIM = Some(pc.ITM) };
    

    let (mut rcc_status, rcc) = rcc.constrain();

    

    rcc_status.is_sysclk_hsi();

    let clock = rcc.freeze(&rcc_status, flash, power);

    let mut gpiob = p.GPIOB.split(&mut rcc_status);
    let mut gpioc = p.GPIOC.split(&mut rcc_status);
    let mut gpiod = p.GPIOD.split(&mut rcc_status);

    // iprintln!(stim, "PLL setup as SYSCLK");
    // iprintln!(stim, "Is sysclck hsi? {}", rcc_status.is_sysclk_hsi());



    
    let mut ledRed = gpiob.pb14.into_push_pull_output(&mut gpiob.gpio);
    let mut ledBlue = gpiob.pb7.into_push_pull_output(&mut gpiob.gpio);

    ledRed.set_high();
    unsafe { LED_RED = Some(ledRed) };

    ledBlue.set_high();

    let serial = Serial::usart3(p.USART3, gpiod.pd8.into_usart3_tx(&mut gpiod.gpio), gpiod.pd9.into_usart3_rx(&mut gpiod.gpio), 115200.bps(), clock, &mut rcc_status);
    let (mut tx, _) = serial.split();

    let mut i2c = I2c::i2c1(p.I2C1, gpiob.pb8.into_i2c1_scl(&mut gpiob.gpio), gpiob.pb9.into_i2c1_sda(&mut gpiob.gpio), &mut rcc_status);

    write_str(&mut tx, "Hello\n\r");

    let mut hts221 = Hts221::new(i2c).unwrap();
    let temp = hts221.read_temperature().unwrap();


    let mut str_fmt: [u8; 100] = [0; 100];

    write!(Wrapper::new(&mut str_fmt), "Temp C: {}\n\r", temp);

    write_buff(& mut tx, &str_fmt);

    let mut tim6 = p.TIM6.constrain();
    rcc_status.enable_tim6();
    tim6.auto_reload_counter(500); // 1Hz
    tim6.prescaler(64_000);  // 500 Hz
    tim6.enable_update_interupt();
    tim6.enable_timer();

    unsafe {TIMER = Some(tim6)};

    //  unsafe {cortex_m::interrupt::enable(); }
    pc.NVIC.enable(stm32f7::stm32f7x7::Interrupt::TIM6_DAC);

    loop {
        // application logic
    }
}
